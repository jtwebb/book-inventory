<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->addCollection(include __DIR__ . '/routes/location.php');
$collection->addCollection(include __DIR__ . '/routes/trip.php');

/**
 * @see \FathomFire\Controller\ErrorController::catchAll
 */
$collection->add('location_404', new Route(
    '/{catchall}',
    ['_controller' => "\\FathomFire\\Controller\\ErrorController::catchAll"],
    ['catchall' => '.+']
));

return $collection;
