<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'location_name_blank_error' => 'The location name cannot be blank.',
    'location_address_blank_error' => 'The address cannot be blank.',
    'location_uuid_blank_error' => 'The UUID cannot be blank.',
    'not_found_by_id' => 'The {{table}} cannot be found by uuid {{uuid}}.',
    'trip_not_found_by_id' => 'The trip cannot be found by uuid {{uuid}}.',
    '404_error' => 'The path {{path}} cannot be found.',
    'database_no_record' => 'The record {{value}} in {{column}} could not be found in the table {{table}}.',
    'database_duplicate' => 'Duplicate UUID. If the name and address are identical to another location, it will generate the same UUID.',
];
