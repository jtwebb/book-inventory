<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$controller = '\\FathomFire\\Controller\\LocationController::';

$routes = new RouteCollection();

/**
 * Gather all locations
 *
 * @see \FathomFire\Controller\LocationController::getAllLocations
 */
$routes->add('location', new Route(
    '/locations/{page}/{number}',
    ['_controller' => "{$controller}getAllLocations", 'page' => 1, 'number' => 20],
    ['page' => '[\d]+', 'number' => '[\d]+']
));

/**
 * Get location by UUID
 *
 * @param string $uuid The UUID of the location you're looking for
 * @see \FathomFire\Controller\LocationController::getLocationById
 */
$routes->add('location_id', new Route(
    '/locations/id/{uuid}',
    ['_controller' => "{$controller}getLocationById"],
    ['uuid' => '[a-zA-Z0-9-]+']
));

/**
 * @see \FathomFire\Controller\LocationController::getLocationByLatitude
 */
$routes->add('location_lat', new Route(
    '/locations/lat/{lat}',
    ['_controller' => "{$controller}getLocationsByLatitude"],
    ['lat' => '[\d-\.]+']
));

/**
 * @see \FathomFire\Controller\LocationController::getLocationByLongitude
 */
$routes->add('location_lng', new Route(
    '/locations/lng/{lng}',
    ['_controller' => "{$controller}getLocationsByLongitude"],
    ['lng' => '[\d-\.]+']
));

/**
 * @see \FathomFire\Controller\LocationController::getLocationByLatLng
 */
$routes->add('location_lat_lng', new Route('/locations/address/{lat}/{lng}', [
    '_controller' => "{$controller}getLocationsByLatLng",
    'lat' => '[\d-\.]+',
    'lng' => '[\d-\.]+',
]));

/**
 * @see \FathomFire\Controller\LocationController::getLocationByTripId
 */
$routes->add('location_trip_id', new Route(
    '/locations/trip/{uuid}',
    ['_controller' => "{$controller}getLocationByTripId"],
    ['uuid' => '[a-zA-Z0-9-]+']
));

/**
 * @see \FathomFire\Controller\LocationController::postNewLocation
 */
$routes->add('location_add', new Route('/locations/add', [
    '_controller' => "{$controller}postNewLocation"
], [], [], '', [], ['POST']));

/**
 * @see \FathomFire\Controller\LocationController::putUpdateLocation
 */
$routes->add('location_update', new Route('/locations/update', [
    '_controller' => "{$controller}putUpdateLocation"
], [], [], '', [], ['PUT']));

/**
 * @see \FathomFire\Controller\LocationController::putDeleteLocation
 */
$routes->add('location_delete', new Route('/locations/delete', [
    '_controller' => "{$controller}putDeleteLocation"
], [], [], '', [], ['DELETE']));

return $routes;
