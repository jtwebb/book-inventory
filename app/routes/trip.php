<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$controller = '\\FathomFire\\Controller\\TripController::';

$routes = new RouteCollection();

/**
 * Gather all trips
 *
 * @see \FathomFire\Controller\TripController::getAllTrips
 */
$routes->add('trip', new Route(
    '/trips/{page}/{number}',
    ['_controller' => "{$controller}getAllTrips", 'page' => 1, 'number' => 20],
    ['page' => '[\d]+', 'number' => '[\d]+']
));

/**
 * Get trip by UUID
 *
 * @param string $uuid The UUID of the trip you're looking for
 * @see \FathomFire\Controller\TripController::getTripById
 */
$routes->add('trip_id', new Route(
    '/trips/id/{uuid}',
    ['_controller' => "{$controller}getTripById"],
    ['uuid' => '[a-zA-Z0-9-]+']
));

/**
 * @see \FathomFire\Controller\TripController::getTripByBookId
 */
$routes->add('trip_book', new Route(
    '/trips/books/id/{uuid}',
    ['_controller' => "{$controller}getTripByBookId"],
    ['uuid' => '[a-zA-Z0-9-]+']
));

/**
 * @see \FathomFire\Controller\TripController::getTripByLocationId
 */
$routes->add('trip_location', new Route(
    '/trips/locations/id/{uuid}',
    ['_controller' => "{$controller}getTripByLocationId"],
    ['uuid' => '[a-zA-Z0-9-]+']
));

/**
 * @see \FathomFire\Controller\TripController::getTripByLongitude
 */
$routes->add('trip_date', new Route(
    '/trips/date/{date}',
    ['_controller' => "{$controller}getTripsByLongitude"],
    ['date' => '[\d-]+']
));

/**
 * @see \FathomFire\Controller\TripController::getTripsByDateRange
 */
$routes->add('trip_date_range', new Route('/trips/dates/{start}/{end}/{sort}', [
    '_controller' => "{$controller}getTripsByDateRange",
    'end' => 'now',
    'sort' => 'asc'
], ['start' => '[\d-]+', 'end' => '[\d-]+|now', 'sort' => 'asc|desc']));

/**
 * @see \FathomFire\Controller\TripController::getTripByTripId
 */
$routes->add('trip_trip_id', new Route(
    '/trips/trip/{uuid}',
    ['_controller' => "{$controller}getTripByTripId"],
    ['uuid' => '[a-zA-Z0-9-]+']
));

/**
 * @see \FathomFire\Controller\TripController::postNewTrip
 */
$routes->add('trip_add', new Route('/trips/add', [
    '_controller' => "{$controller}postNewTrip"
], [], [], '', [], ['POST']));

/**
 * @see \FathomFire\Controller\TripController::putUpdateTrip
 */
$routes->add('trip_update', new Route('/trips/update', [
    '_controller' => "{$controller}putUpdateTrip"
], [], [], '', [], ['PUT']));

/**
 * @see \FathomFire\Controller\TripController::putDeleteTrip
 */
$routes->add('trip_delete', new Route('/trips/delete', [
    '_controller' => "{$controller}putDeleteTrip"
], [], [], '', [], ['DELETE']));

return $routes;
