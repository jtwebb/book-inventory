<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'database' => [
        'dbname'   => 'book_inventory',
        'user'     => 'root',
        'password' => '',
        'host'     => 'localhost',
        'driver'   => 'pdo_mysql',
        'charset'  => 'utf8mb4',
    ],

    'map' => [
        'google' => [
            'api_key' => 'AIzaSyAgc_0Pfql0te8RS83s0SZ7vGW8U802Sns',
            'api_url' => 'https://maps.google.com/maps/api/geocode/json'
        ],
    ],
];
