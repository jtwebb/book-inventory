<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

defined('BASE_DIR') || define('BASE_DIR', dirname(__DIR__));
defined('LOG_DIR') || define('LOG_DIR', BASE_DIR . '/var/logs');
defined('CACHE_DIR') || define('CACHE_DIR', BASE_DIR . '/var/cache');
defined('DEBUG') || define('DEBUG', true);
