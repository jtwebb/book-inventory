<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Entity;

class Location extends AbstractEntity
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var string
     */
    protected $uuid;

    public function getId()
    {
        return $this->id;
    }

    public function withId($id)
    {
        return $this->with('id', $id);
    }

    public function getName()
    {
        return $this->name;
    }

    public function withName($name)
    {
        return $this->with('name', $name);
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function withAddress($address)
    {
        return $this->with('address', $address);
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function withLatitude($latitude)
    {
        return $this->with('latitude', $latitude);
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function withLongitude($longitude)
    {
        return $this->with('longitude', $longitude);
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function withUuid($uuid)
    {
        return $this->with('uuid', $uuid);
    }
}
