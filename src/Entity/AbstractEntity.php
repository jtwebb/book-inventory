<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Entity;

abstract class AbstractEntity implements EntityInterface
{
    protected function with($property, $value)
    {
        if ($value === $this->{$property}) {
            return $this;
        }

        $new = clone $this;
        $new->{$property} = $value;
        return $new;
    }
}
