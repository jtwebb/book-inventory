<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Map;

use FathomFire\Config\Config;

class GoogleApi
{
    /**
     * @param $address
     *
     * @return array
     */
    public function getLatLong($address)
    {
        $response = $this->getResponse([
            'address' => $address,
            'sensor' => false,
            'key' => Config::get('map.google.api_key'),
        ]);

        if ($response->status === 'OK') {
            return [$response->results[0]->geometry->location->lat, $response->results[0]->geometry->location->lng];
        }

        return [null, null];
    }

    /**
     * @param array $queryString
     *
     * @return object
     */
    protected function getResponse(array $queryString = [])
    {
        $queryString = http_build_query($queryString);
        $response = file_get_contents(Config::get('map.google.api_url') . '?' . $queryString);
        return json_decode($response);
    }
}
