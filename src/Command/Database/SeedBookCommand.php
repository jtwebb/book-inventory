<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Command\Database;

use Faker\Factory;
use FathomFire\Database\Manager;
use FathomFire\Database\Seed;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SeedBookCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('db:seed:book')
            ->setDescription('Seeds the book table with dummy data')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $seeder = new Seed(Factory::create(), new Manager());
        $seeder->seedBook();
        $output->writeln("Book Seeded\n");
    }
}
