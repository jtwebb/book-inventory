<?php
/**
 * Created by PhpStorm.
 * User: jtwebb
 * Date: 7/22/2015
 * Time: 9:18 PM
 */

namespace FathomFire\Command\Database;

use Faker\Factory;
use FathomFire\Database\Manager;
use FathomFire\Database\Seed;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SeedLocationCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('db:seed:location')
            ->setDescription('Seeds the location table with dummy data')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $seeder = new Seed(Factory::create(), new Manager());
        $seeder->seedLocation();
        $output->writeln("Locations Seeded\n");
    }
}