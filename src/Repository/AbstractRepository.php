<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Repository;

use FathomFire\Config\String;
use FathomFire\Pagination\PaginationFactory;
use FathomFire\Transformers\TransformerFactory;

abstract class AbstractRepository
{
    /**
     * @var \Doctrine\DBAL\Query\QueryBuilder
     */
    protected $builder;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * @var array
     */
    protected $fields;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var \FathomFire\Pagination\Pagination
     */
    protected $pagination;

    /**
     * @return array
     */
    abstract protected function getFields();

    /**
     * @return string
     */
    abstract protected function getTable();

    public function __construct()
    {
        $this->customErrors = RepositoryFactory::createCustomErrors();
        $dbManager = RepositoryFactory::createManager();
        $this->builder = $dbManager->getBuilder();
        $this->connection = $dbManager->getConnection();
        $this->table = $this->getTable();
        $this->fields = $this->getFields();
        $this->pagination = PaginationFactory::create($this->table);
    }

    /**
     * @param Integer $page    The current page
     * @param Integer $number  The number of items per page
     *
     * @return array
     */
    protected function getAll($page, $number)
    {
        list($pagination, $finishQuery) = $this->pagination->paginate($page, $number);

        $items =  $this->connection->fetchAll(
            "SELECT {$this->formatFields()} FROM {$this->table} {$finishQuery}"
        );
        $items = TransformerFactory::create($this->table, $items);

        return [
            'data' => $items,
            'pagination' => $pagination
        ];
    }

    /**
     * @param String  $uuid   The UUID for the trip we're searching for
     *
     * @return array
     */
    protected function getById($uuid)
    {
        $item = $this->connection->fetchAssoc(
            "SELECT {$this->formatFields()} FROM {$this->table} WHERE `uuid` = :uuid",
            ['uuid' => $uuid]
        );
        if (!$item) {
            return ['data' =>
                ['error' => String::get('not_found_by_id', ['{{table}}' => $this->table, '{{uuid}}' => $uuid])]
            ];
        }

        $item = TransformerFactory::create($this->table, $item, false);

        return ['data' => $item];
    }

    protected function getByRelatedId($uuid, $childTable)
    {
        $where = "`uuid` = (SELECT `{$this->table}_id` FROM {$childTable} WHERE `uuid` = :uuid)";
        return $this->getBy($where, ['uuid' => $uuid]);
    }

    protected function getBy($where, array $params)
    {
        $items = $this->connection->fetchAll(
            "SELECT {$this->formatFields()}
             FROM {$this->table}
             WHERE {$where}",
            $params
        );

        if (!$items) {
            return ['data' => ['error' => String::get(
                'not_found_by_id',
                ['{{table}}' => $this->table, '{{uuid}}' => array_key_exists('uuid', $params) ? $params['uuid'] : '']
            )]];
        }

        $items = TransformerFactory::create($this->table, $items);

        return ['data' => $items];
    }

    protected function create($input)
    {
        $values = [];
        $params = [];
        $i = 0;
        foreach ($this->fields as $field) {
            $values[$field] = '?';
            $params[$i] = $input[$field];
            $i++;
        }

        try {
            $result = $this->connection->createQueryBuilder()
                ->insert($this->table)
                ->values($values);

            foreach ($params as $key => $value) {
                $result->setParameter($key, $value);
            }

            $result->execute();

            $insertId = (int)$this->connection->lastInsertId();
        } catch (\Exception $e) {
            return ['data' => ['error' => $this->customErrors->duplicateKey(
                $e->getCode(),
                String::get('database_duplicate')
            )]];
        }

        return ['data' => $this->getBy('`id` = :id', ['id' => $insertId])];
    }

    protected function update($input)
    {
        try {
            $uuid = $input['uuid'];
            unset($input['uuid']);

            $result = $this->connection->createQueryBuilder()
                ->update($this->table);

            $i = 0;
            foreach ($input as $key => $value) {
                $result->set($key, '?')
                    ->setParameter($i, $value)
                ;
                $i++;
            }

            $result = $result->where('uuid = ?')
                ->setParameter($i, $uuid)
                ->execute()
            ;
        } catch (\Exception $e) {
            return ['data' => ['error' => $e->getMessage()]];
        }

        return ['data' => ['rows_affected' => $result]];
    }

    protected function delete($uuid)
    {
        try {
            $result = $this->connection->delete($this->table, ['uuid' => $uuid]);
        } catch (\Exception $e) {
            $message = String::get('database_no_record', [
                '{{value}}' => $uuid, '{{column}}' => 'uuid', '{{table}}' => $this->table
            ]);

            return ['data' => ['error' => $this->customErrors->noRecord($e->getCode(), $message)]];
        }

        if (!$result) {
            return ['data' => ['error' => String::get('not_found_by_id', [
                'table' => $this->table,
                '{{uuid}}' => $uuid
            ])]];
        }

        return ['data' => ['rows_affected' => $result]];
    }

    /**
     * Setup the fields for use in SQL query
     *
     * @return string
     */
    protected function formatFields()
    {
        return implode(', ', array_map(function ($field) {
            return '`' . $field . '`';
        }, $this->fields));
    }
}
