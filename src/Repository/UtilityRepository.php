<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Repository;

class UtilityRepository
{
    protected $connection;

    public function __construct()
    {
        $this->connection = RepositoryFactory::createManager()->getConnection();
    }

    /**
     * @param             $table
     * @param string|null $where A query string
     *
     * @return mixed
     */
    public function getCount($table, $where = null)
    {
        $query = "SELECT COUNT(id) as `count` FROM {$table}";
        if ($where !== null) {
            $query .= ' WHERE ' .  $where;
        }

        return $this->connection->fetchAssoc($query)['count'];
    }
}
