<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Repository;

use FathomFire\Database\Manager;
use FathomFire\Http\CustomErrors;

class RepositoryFactory
{
    /**
     * @return \FathomFire\Database\Manager
     */
    public static function createManager()
    {
        return new Manager();
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public static function createRepository($name)
    {
        $name = ucfirst($name);
        $class = "\\FathomFire\\Repository\\{$name}Repository";

        if (!class_exists($class)) {
            throw new \InvalidArgumentException("The class {$class} does not exist.");
        }

        return new $class();
    }

    /**
     * @return \FathomFire\Http\CustomErrors
     */
    public static function createCustomErrors()
    {
        return new CustomErrors();
    }
}
