<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Repository;

class LocationRepository extends AbstractRepository
{
    /**
     * Get all locations
     *
     * @param $page
     * @param $number
     *
     * @return array
     */
    public function getLocations($page, $number)
    {
        return $this->getAll($page, $number);
    }

    /**
     * Get location by uuid
     *
     * @param $uuid
     *
     * @return array
     */
    public function getLocationById($uuid)
    {
        return $this->getById($uuid);
    }

    /**
     * @param $lat
     *
     * @return array
     */
    public function getLocationsByLatitude($lat)
    {
        return $this->getBy('`latitude` = :lat', ['lat' => $lat]);
    }

    /**
     * @param $lng
     *
     * @return array
     */
    public function getLocationsByLongitude($lng)
    {
        return $this->getBy('`longitude` = :lng', ['lng' => $lng]);
    }

    /**
     * @param $lat
     * @param $lng
     *
     * @return array
     */
    public function getLocationsByLatLng($lat, $lng)
    {
        return $this->getBy('`latitude` = :lat AND `longitude` = :lng', ['lat'  => $lat, 'lng' => $lng]);
    }

    /**
     * @param $uuid
     *
     * @return array
     */
    public function getLocationByTripId($uuid)
    {
        return $this->getByRelatedId($uuid, 'trip');
    }

    /**
     * @param $input
     *
     * @return array|int
     */
    public function saveLocation($input)
    {
        return $this->create($input);
    }

    /**
     * @param $input
     *
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function updateLocation($input)
    {
        return $this->update($input);
    }

    /**
     * Deletes a location
     *
     * @param $uuid
     *
     * @return array
     */
    public function deleteLocation($uuid)
    {
        return $this->delete($uuid);
    }

    protected function getFields()
    {
        return ['name', 'address', 'uuid', 'latitude', 'longitude'];
    }

    protected function getTable()
    {
        return 'location';
    }
}
