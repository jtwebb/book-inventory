<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Repository;

class TripRepository extends AbstractRepository
{
    /**
     * @var \FathomFire\Repository\LocationRepository
     */
    private $locationRepository;

    public function __construct()
    {
        parent::__construct();
        $this->locationRepository = RepositoryFactory::createRepository('location');
    }

    /**
     * Get all trips
     *
     * @param $page
     * @param $number
     *
     * @return array
     */
    public function getTrips($page, $number)
    {
        $trips = $this->getAll($page, $number);
        $trips = $this->getLocation($trips);

        return $trips;
    }

    /**
     * Get location by uuid
     *
     * @param $uuid
     *
     * @return array
     */
    public function getTripById($uuid)
    {
        $trip = $this->getById($uuid);
        $trip = $this->getLocation(['data' => $trip])['data'];

        return $trip;
    }

    public function getTripByBookId($uuid)
    {
        return $this->getByRelatedId($uuid, 'book');
    }

    public function getTripByLocationId($uuid)
    {
        return $this->getBy('`location_id` = :uuid', ['uuid' => $uuid]);
    }

    public function getTripByDate($date)
    {
        $date = date('Y-m-d', strtotime($date));
        return $this->getBy('date = :date', ['date' => $date]);
    }

    public function getTripByDateRange($start, $end, $sort)
    {
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', ($end === 'now' ? time() : strtotime($end)));

        return $this->getBy(
            "date > :start AND date < :end ORDER BY `date` {$sort}",
            ['start' => $start, 'end' => $end]
        );
    }

    public function saveTrip($input)
    {
        return $this->create($input);
    }

    public function updateTrip($input)
    {
        return $this->update($input);
    }

    public function deleteTrip($uuid)
    {
        return $this->delete($uuid);
    }

    protected function getFields()
    {
        return ['date', 'price', 'receipt', 'location_id', 'uuid'];
    }

    protected function getTable()
    {
        return 'trip';
    }

    /**
     * @param array $trips
     *
     * @return mixed
     */
    private function getLocation(array $trips)
    {
        foreach ($trips['data'] as $key => $trip) {
            if (isset($trip['location_id'])) {
                $location = $this->locationRepository->getLocationById($trip['location_id']);
                $trips['data'][$key]['location'] = $location['data'];
                unset($trips['data'][$key]['location_id']);
            }
        }

        return $trips;
    }
}
