<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Enum;

abstract class ConditionEnum
{
    const POOR = 0;

    const ACCEPTABLE = 1;

    const GOOD = 2;

    const VERY_GOOD = 3;

    const LIKE_NEW = 4;

    const IS_NEW = 5;
}
