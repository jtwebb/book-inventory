<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Enum;

abstract class SaleStatusEnum
{
    /**
     * Sorted, de-stickered, researched, priced
     */
    const PRE_SALE = 0;

    /**
     * Listed
     */
    const FOR_SALE = 1;

    /**
     * Sold and shipped
     */
    const SOLD = 2;
}
