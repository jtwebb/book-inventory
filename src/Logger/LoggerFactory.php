<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Logger;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerFactory
{
    /**
     * @param string $name
     * @param string $fileName
     * @param int    $loggingLevel
     *
     * @return \Monolog\Logger
     */
    public static function create($name = 'ErrorLogger', $fileName = 'error', $loggingLevel = Logger::WARNING)
    {
        $logger = new Logger($name);
        $logger->pushHandler(new StreamHandler(LOG_DIR . "/{$fileName}.log", $loggingLevel));

        return $logger;
    }
}