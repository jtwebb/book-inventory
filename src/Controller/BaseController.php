<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Controller;

use FathomFire\Http\CustomErrors;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseController
{
    protected $customErrors;

    public function __construct()
    {
        $this->customErrors = new CustomErrors();
        $this->setRepo();
    }

    abstract protected function setRepo();

    /**
     * @param     $content
     * @param int $status
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    protected function respond($content, $status = 200)
    {
        $response = new JsonResponse();
        $response->setStatusCode($status);
        $response->setData((array)$content);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param $string
     *
     * @return bool
     */
    protected function isJson($string)
    {
        if (!is_string($string)) {
            return false;
        }

        json_decode($string);
        return (json_last_error() === JSON_ERROR_NONE);
    }

    /**
     * @param string|array $input
     *
     * @return array
     */
    protected function prepare($input)
    {
        if ($this->isJson($input)) {
            return json_decode($input, true);
        }

        return $input;
    }
}
