<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Controller;

use FathomFire\Repository\TripRepository;

class TripController extends BaseController
{
    /**
     * Get all trips
     *
     * Example Response:
     *
     * <code>
     *  [
     *      "data": [
     *          {
     *              "date":"Fri, 26 Jun 2009 00:00:00 -0500",
     *              "price":29.14,
     *              "receipt":"assets/img/receipts/receipt.jpg",
     *              "uuid":"678accbb-e3b0-3460-9b08-b383fa256548",
     *              "location": {
     *                 "name":"VonRueden, Lockman and Heathcote",
     *                 "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *                 "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *                 "latitude":29.1471,
     *                 "longitude":-55.913363
     *             }
     *          },
     *          {
     *              "date":"Mon, 06 Feb 1984 00:00:00 -0600",
     *              "price":267.14,
     *              "receipt":"assets/img/receipts/receipt.jpg",
     *              "uuid":"678accbb-e3b0-3460-9b08-b383fa256548",
     *              "location": {
     *                 "name":"VonRueden, Lockman and Heathcote",
     *                 "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *                 "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *                 "latitude":29.1471,
     *                 "longitude":-55.913363
     *             }
     *          },
     *          {
     *              "date":"Tue, 07 Jun 1977 00:00:00 -0500",
     *              "price":69.77,
     *              "receipt":"assets/img/receipts/receipt.jpg",
     *              "uuid":"678accbb-e3b0-3460-9b08-b383fa256548",
     *              "location": {
     *                 "name":"VonRueden, Lockman and Heathcote",
     *                 "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *                 "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *                 "latitude":29.1471,
     *                 "longitude":-55.913363
     *             }
     *          }
     *      ]
     *      "pagination": {
     *          "total": 29,
     *          "current_page": 1,
     *          "per_page": 20,
     *          "total_pages": 3,
     *          "next_url": "trips/1/20",
     *          "prev_url": "",
     *      }
     *  ]
     * </code>
     *
     * @param $page
     * @param $number
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllTrips($page, $number)
    {
        return $this->respond($this->repo->getTrips((int)$page, (int)$number));
    }

    /**
     * Example Response:
     *
     * <code>
     *  "data": [
     *      {
     *          "date":"Fri, 26 Jun 2009 00:00:00 -0500",
     *          "price":29.14,
     *          "receipt":"assets/img/receipts/receipt.jpg",
     *          "uuid":"678accbb-e3b0-3460-9b08-b383fa256548",
     *          "location": {
     *             "name":"VonRueden, Lockman and Heathcote",
     *             "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *             "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *             "latitude":29.1471,
     *             "longitude":-55.913363
     *         }
     *      }
     *  ]
     * </code>
     * @param $uuid
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTripById($uuid)
    {
        return $this->respond($this->repo->getTripById($uuid));
    }

    public function getTripByBookId($uuid)
    {
        return $this->respond($this->repo->getTripByBookId($uuid));
    }

    public function getTripByLocationId($uuid)
    {
        return $this->respond($this->repo->getTripByLocationId($uuid));
    }

    public function getTripsByDateRange($start, $end, $sort)
    {
        return $this->respond($this->repo->getTripByDateRange($start, $end, $sort));
    }

    protected function setRepo()
    {
        $this->repo = new TripRepository();
    }
}
