<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Controller;

use FathomFire\Config\String;
use FathomFire\Http\ResponseCodes;
use Symfony\Component\HttpFoundation\Request;

class ErrorController extends BaseController
{
    /**
     * 404 Error
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function catchAll(Request $request)
    {
        return $this->respond(
            ['data' => ['error' => String::get('404_error', ['{{path}}' => $request->getUri()])]],
            ResponseCodes::NOT_FOUND
        );
    }

    protected function setRepo()
    {
        return false;
    }
}
