<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Controller;

use FathomFire\Http\ResponseCodes;
use FathomFire\Map\MapFactory;
use FathomFire\Repository\LocationRepository;
use FathomFire\Validation\ValidationFactory;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

class LocationController extends BaseController
{
    /**
     * Get all locations
     *
     * Example Response:
     *
     * <code>
     *  [
     *      "data": [
     *          {
     *              "name":"Leuschke and Sons",
     *              "address":"1417 Grant Lodge\nLake Rae, CT 41561",
     *              "uuid":"f999e22f-e05a-3192-ae08-2eca644a10d2",
     *              "latitude":"29.1471",
     *              "longitude":"-55.913363"
     *          },
     *          {
     *              "name":"Bechtelar, Renner and Lockman",
     *              "address":"48833 Donavon Crest Apt. 493\nEast Morris, RI 35842",
     *              "uuid":"678accbb-e3b0-3460-9b08-b383fa256548",
     *              "latitude":"29.1471",
     *              "longitude":"-55.913363"
     *          },
     *          {
     *              "name":"Funk, Mueller and Rempel",
     *              "address":"1728 Lebsack Landing Suite 567\nLake Dasia, RI 25728",
     *              "uuid":"306eea01-d1bb-31d1-a3c0-5d6438111aa4",
     *              "latitude":"29.1471",
     *              "longitude":"-55.913363"
     *          }
     *      ]
     *      "pagination": {
     *          "total": 29,
     *          "current_page": 1,
     *          "per_page": 20,
     *          "total_pages": 3,
     *          "next_url": "locations/1/20",
     *          "prev_url": "",
     *      }
     *  ]
     * </code>
     *
     * @param $page
     * @param $number
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllLocations($page, $number)
    {
        return $this->respond($this->repo->getLocations((int)$page, (int)$number));
    }

    /**
     * Get location by UUID
     *
     * Example Response:
     *
     * <code>
     *  {
     *      "data": {
     *          "name":"O'Reilly Ltd",
     *          "address":"83069 Weldon Turnpike Apt. 709 North Asia, VT 33005",
     *          "uuid":"f3771575-f77a-3ec5-a82a-9738975162f9",
     *          "latitude":"29.1471",
     *          "longitude":"-55.913363"
     *      }
     *  }
     * </code>
     *
     * @param $uuid
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getLocationById($uuid)
    {
        return $this->respond($this->repo->getLocationById($uuid));
    }

    /**
     * Get locations by latitude
     *
     * Example Response:
     *
     *  [
     *      {
     *          "name":"VonRueden, Lockman and Heathcote",
     *          "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *          "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *          "latitude":"29.1471",
     *          "longitude":"-55.913363"
     *      }
     *  ]
     *
     * @param $lat
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getLocationsByLatitude($lat)
    {
        return $this->respond($this->repo->getLocationsByLatitude($lat));
    }

    /**
     * Get locations by longitude
     *
     * Example Response:
     *
     *  [
     *      {
     *          "name":"VonRueden, Lockman and Heathcote",
     *          "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *          "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *          "latitude":"29.1471",
     *          "longitude":"-55.913363"
     *      }
     *  ]
     *
     * @param $lng
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getLocationsByLongitude($lng)
    {
        return $this->respond($this->repo->getLocationsByLongitude($lng));
    }

    /**
     * Get locations by latitude and longitude
     *
     * Example Response:
     *
     *  [
     *      "data": [
     *          {
     *              "name":"VonRueden, Lockman and Heathcote",
     *              "address":"8937 Mills Valley\nNew Lessie, WA 65813",
     *              "uuid":"36a3a148-b311-3e07-89a5-88f6d6faf446",
     *              "latitude":"29.1471",
     *              "longitude":"-55.913363"
     *          }
     *      ]
     *  ]
     *
     * @param $lat
     * @param $lng
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getLocationsByLatLng($lat, $lng)
    {
        return $this->respond($this->repo->getLocationsByLatLng($lat, $lng));
    }

    /**
     * Get location by trip id
     *
     * Example Response:
     *
     *  [
     *      "data":
     *          {
     *              "name":"O'Reilly Ltd",
     *              "address":"83069 Weldon Turnpike Apt. 709 North Asia, VT 33005",
     *              "uuid":"f3771575-f77a-3ec5-a82a-9738975162f9",
     *              "latitude":"29.1471",
     *              "longitude":"-55.913363"
     *          }
     *  ]
     *
     * @param $uuid
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getLocationByTripId($uuid)
    {
        return $this->respond($this->repo->getLocationByTripId($uuid));
    }

    /**
     * Update a location
     *
     * Example Response:
     *
     *  [
     *      "data": [
     *          "rows_affected": 1
     *      ]
     *  ]
     *
     * Or on Error:
     *
     *  [
     *      "data": [
     *          "error": "error message"
     *      ]
     *  ]
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putUpdateLocation(Request $request)
    {
        $validation = ValidationFactory::create('location');
        $input = $request->request->all();

        if ($validation->isValid($input, true)) {
            $location = $this->repo->getLocationById($input['uuid']);

            if (array_key_exists('error', $location['data'])) {
                return $this->respond($location, ResponseCodes::BAD_REQUEST);
            }

            $input = array_merge($location['data'], $input);

            if (!$input['latitude'] || $location['data']['address'] !== $input['address']) {
                $mapApi = MapFactory::createApi();
                list($input['latitude'], $input['longitude']) = $mapApi->getLatLong($input['address']);
            }

            $location = $this->repo->updateLocation($input);
            return $this->respond(
                $location,
                array_key_exists('error', $location['data']) ? ResponseCodes::BAD_REQUEST : ResponseCodes::OK
            );
        }

        return $this->respond($validation->getViolations(), ResponseCodes::BAD_REQUEST);
    }

    /**
     * Add a location
     *
     * Example Response:
     *
     * <code>
     *  [
     *      "data": [
     *          "insert_id": 53
     *      ]
     *  ]
     * </code>
     *
     * Or on Error:
     *
     * <code>
     *  [
     *      "data": [
     *          "error": "error message"
     *      ]
     *  ]
     * </code>
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function postNewLocation(Request $request)
    {
        $validation = ValidationFactory::create('location');
        $input = $request->request->all();

        if ($validation->isValid($input)) {
            if (!$input['latitude']) {
                $mapApi = MapFactory::createApi();
                list($input['latitude'], $input['longitude']) = $mapApi->getLatLong($input['address']);
            }

            $uuidSeed = '';
            foreach ($input as $string) {
                $uuidSeed .= $string;
            }

            $input['uuid'] = Uuid::uuid5(Uuid::NAMESPACE_DNS, $uuidSeed)
                ->toString()
            ;

            $location = $this->repo->saveLocation($input);
            return $this->respond(
                $location,
                array_key_exists('error', $location['data']) ? ResponseCodes::BAD_REQUEST : ResponseCodes::CREATED_OK
            );
        }

        return $this->respond($validation->getViolations(), ResponseCodes::BAD_REQUEST);
    }

    public function putDeleteLocation(Request $request)
    {
        $input = $request->request->get('uuid');
        $location = $this->repo->deleteLocation($input);

        return $this->respond(
            $location,
            array_key_exists('error', $location['data']) ? ResponseCodes::BAD_REQUEST : ResponseCodes::OK
        );
    }

    /**
     * Sets the repo we're pulling data from
     */
    protected function setRepo()
    {
        $this->repo = new LocationRepository();
    }
}
