<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Http;

class ErrorCode
{
    /**
     * a unique identifier for this particular occurrence of the problem
     *
     * @var string
     */
    protected $id;

    /**
     * the HTTP status code applicable to this problem, expressed as a string value.
     *
     * @var string
     */
    protected $status;

    /**
     * an application-specific error code, expressed as a string value.
     *
     * @var string
     */
    protected $code;

    /**
     * a short, human-readable summary of the problem that SHOULD NOT change from
     * occurrence to occurrence of the problem, except for purposes of localization.
     *
     * @var string
     */
    protected $title;

    /**
     * a human-readable explanation specific to this occurrence of the problem.
     *
     * @var string
     */
    protected $detail;

    public function toArray()
    {
        return get_object_vars($this);
    }

    public static function createError()
    {
        return new self();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function setDetail($detail)
    {
        $this->detail = $detail;
        return $this;
    }
}
