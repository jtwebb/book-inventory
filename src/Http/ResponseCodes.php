<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Http;

class ResponseCodes
{
    /**
     * Generic everything is OK
     */
    const OK = 200;

    /**
     * Created something OK
     */
    const CREATED_OK = 201;

    /**
     * Accepted but is being processed async (for a video means encoding, for image means resizing, etc.
     */
    const BEING_PROCESSED = 202;

    /**
     * Bad Request (should really be for invalid syntax, but some folks use for validation)
     */
    const BAD_REQUEST = 400;

    /**
     * Unauthorized (no current user and there should be)
     */
    const UNAUTHORIZED = 401;

    /**
     * The current user is forbidden from accessing this data
     */
    const FORBIDDEN = 403;

    /**
     * That URL is not a valid route, or the item resource does not exist
     */
    const NOT_FOUND = 404;

    /**
     * Data has been deleted, deactivated, suspended, etc.
     */
    const GONE = 410;

    /**
     * Method Not Allowed
     */
    const NOT_ALLOWED = 405;

    /**
     * Something unexpected happened and it is the APIs fault
     */
    const SERVER_ERROR = 500;

    /**
     * API is not here right now, please try again later
     */
    const NOT_HERE = 503;

    private function __construct()
    {
    }
}