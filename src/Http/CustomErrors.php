<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Http;

use FathomFire\Debug\DebugFactory;

class CustomErrors
{
    /**
     * @var \FathomFire\Debug\Debug
     */
    protected $debug;

    public function __construct()
    {
        $this->debug = DebugFactory::create();
    }

    /**
     * @param       $errorMethod
     * @param array $params
     *
     * @return bool|string
     * @throws \Exception
     */
    public function get($errorMethod, array $params = [])
    {
        if (method_exists($this, $errorMethod)) {
            return call_user_func_array($errorMethod, $params);
        }

        $this->debug->errorException(new \InvalidArgumentException(
            "The method {$errorMethod} does not exist in " . __CLASS__
        ));

        return false;
    }

    /**
     * @param int    $errorCode
     * @param string $message
     *
     * @return array
     */
    public function duplicateKey($errorCode, $message)
    {
        return ErrorCode::createError()
            ->setId($errorCode)
            ->setStatus(ResponseCodes::SERVER_ERROR)
            ->setCode('DB_Duplication')
            ->setTitle('Duplicate Entry For Unique Field in Database')
            ->setDetail($message)
            ->toArray()
        ;
    }

    /**
     * @param int    $errorCode
     * @param string $message
     *
     * @return array
     */
    public function noRecord($errorCode, $message)
    {
        return ErrorCode::createError()
            ->setId($errorCode)
            ->setStatus(ResponseCodes::BAD_REQUEST)
            ->setCode('No_Record')
            ->setTitle('No database record')
            ->setDetail($message)
            ->toArray()
        ;
    }

    /**
     * @param string $message
     *
     * @return array
     */
    public function notFound($message)
    {
        return ErrorCode::createError()
            ->setStatus(ResponseCodes::NOT_FOUND)
            ->setCode('Resource_Not_Found')
            ->setTitle('Resource Not Found')
            ->setDetail($message)
            ->toArray()
        ;
    }
}
