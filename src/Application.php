<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire;

use FathomFire\Controller\ControllerResolver;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\PhpFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

class Application
{
    /**
     * @var \Symfony\Component\Routing\Loader\PhpFileLoader
     */
    protected $loader;

    /**
     * @var string
     */
    protected $routeFile;

    /**
     * @var \Symfony\Component\Routing\RouteCollection
     */
    protected $routes;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var \Callable
     */
    protected $controller;

    /**
     * @var array
     */
    protected $arguments;

    /**
     * @param string|null                                           $routeFile
     * @param \Symfony\Component\Config\Loader\LoaderInterface|null $loader
     */
    public function __construct($routeFile = null, LoaderInterface $loader = null)
    {
        if (null !== $routeFile) {
            $this->routeFile = $routeFile;
        } else {
            $this->routeFile = 'routes.php';
        }

        if (null !== $loader) {
            $this->loader = $loader;
        } else {
            $this->locator = new FileLocator(BASE_DIR . '/app');
            $this->loader = new PhpFileLoader($this->locator);
        }

        $this->routes = $this->loader->load($this->routeFile);
    }

    /**
     * Setup the request items and route
     */
    public function setupRequest()
    {
        $this->request = Request::createFromGlobals();
        $context = new RequestContext();
        $context->fromRequest($this->request);
        $matcher = new UrlMatcher($this->routes, $context);
        $this->request->attributes->add($matcher->match($this->request->getPathInfo()));
        $resolver = new ControllerResolver();
        $this->controller = $resolver->getController($this->request);
        $this->arguments = $resolver->getArguments($this->request, $this->controller);
    }

    /**
     * Send the response to the browser
     */
    public function send()
    {
        $response = call_user_func_array($this->controller, $this->arguments);
        $response->prepare($this->request);
        $response->send();
    }
}
