<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Pagination;

use FathomFire\Repository\RepositoryFactory;

class Pagination
{
    protected $table;

    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * @param Integer $page   The current page number
     * @param Integer $count  Total count of items
     * @param Integer $number Number per page
     *
     * @return string
     */
    protected function nextUrl($page, $count, $number)
    {
        return $page < (ceil($count / $number)) && $page > 0
            ? '/locations/' . ($page + 1) . '/' . $number
            : '';
    }

    /**
     * @param Integer $page   The current page number
     * @param Integer $count  Total count of items
     * @param Integer $number Number per page
     *
     * @return string
     */
    protected function prevUrl($page, $count, $number)
    {
        return $page > 1 && $page <= (ceil($count / $number))
            ? '/locations/' . ($page - 1) . '/' . $number
            : '';
    }

    /**
     * Makes sure the current page is greater than zero
     *
     * @param Integer $page The current page
     *
     * @return int
     */
    protected function normalizePage($page)
    {
        return $page < 1 ? 1 : (int)$page;
    }

    /**
     * Makes sure the number per page is greater than zero and less than 100
     * @param Integer $number
     *
     * @return int
     */
    protected function normalizeNumber($number)
    {
        return $number > 100 ? 100 : ($number < 1 ? 1 : (int)$number);
    }

    /**
     * @param Integer     $page   The current page
     * @param Integer     $number The number of items per page
     * @param string|null $where  A query string
     *
     * @return array
     */
    public function paginate($page, $number, $where = null)
    {
        $utilRepo = RepositoryFactory::createRepository('utility');
        $count  = $utilRepo->getCount($this->table, $where);
        $number = $this->normalizeNumber($number);
        $page   = $this->normalizePage($page);

        $pagination = [
            'total'        => (int)$count,
            'current_page' => (int)$page,
            'per_page'     => (int)$number,
            'total_pages'  => (int)ceil($count / $number),
            'next_url'     => $this->nextUrl($page, $count, $number),
            'prev_url'     => $this->prevUrl($page, $count, $number)
        ];

        return [$pagination, $this->getPaginationQuery($pagination)];
    }

    /**
     * @param array $pagination
     *
     * @return string
     * @throws \InvalidArgumentException if 'per_page' and|or 'current_page' aren't keys in $pagination
     */
    protected function getPaginationQuery(array $pagination)
    {
        $required = ['per_page', 'current_page'];
        foreach ($required as $require) {
            if (!array_key_exists($require, $pagination)) {
                throw new \InvalidArgumentException(
                    'You must pass in an array that contains the keys `per_page` and `current_page`'
                );
            }
        }

        return "LIMIT {$pagination['per_page']} OFFSET "
        . (($pagination['current_page'] - 1) * $pagination['per_page']);
    }
}
