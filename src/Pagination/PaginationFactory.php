<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Pagination;

class PaginationFactory
{
    /**
     * @param $table
     *
     * @return \FathomFire\Pagination\Pagination
     */
    public static function create($table)
    {
        return new Pagination($table);
    }
}
