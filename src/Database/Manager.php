<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Database;

use Doctrine\DBAL\DriverManager;
use FathomFire\Config\Config;

class Manager
{
    protected $connectionParams;

    /**
     * @param array $connectionParams
     */
    public function __construct(array $connectionParams = [])
    {
        $this->connectionParams = empty($connectionParams)
            ? Config::get('database')
            : $connectionParams
        ;
    }

    /**
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getConnection()
    {
        return DriverManager::getConnection($this->connectionParams);
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getBuilder()
    {
        return $this->getConnection()->createQueryBuilder();
    }
}