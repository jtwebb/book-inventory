<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Database;

use Faker\Generator;

class Seed
{
    /**
     * @var \Faker\Generator
     */
    protected $generator;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    protected $locationUuids = [];

    protected $tripUuids = [];

    /**
     * @param \Faker\Generator             $generator
     * @param \FathomFire\Database\Manager $dbManager
     */
    public function __construct(Generator $generator, Manager $dbManager)
    {
        $this->generator  = $generator;
        $this->connection = $dbManager->getConnection();
    }

    /**
     * Seeds the book table
     *
     * @throws \Exception
     */
    public function seedBook()
    {
        // Truncate table
        $this->truncate('book', function ($table) {
            // Then add new data
            for ($i = 0; $i <= 120; $i++) {
                $askingPrice = $this->getPrice(80);
                $soldPrice   = $this->getPrice(floor($askingPrice));


                $sold = rand(0, 1);
                list($soldDate, $soldPrice) = $this->getSoldDetails($sold, $soldPrice);


                $this->connection->createQueryBuilder()
                    ->insert($table)
                    ->values(
                        [
                            'title' => '?',
                            'isbn' => '?',
                            'author' => '?',
                            'year_printed' => '?',
                            'signed' => '?',
                            'first_edition' => '?',
                            'printing' => '?',
                            'trip_id' => '?',
                            'cost' => '?',
                            'market' => '?',
                            'sale_status' => '?',
                            'sale_price' => '?',
                            'firm' => '?',
                            'max_discount' => '?',
                            'book_condition' => '?',
                            'description' => '?',
                            'format' => '?',
                            'sold_on' => '?',
                            'sold_price' => '?',
                            'book_read' => '?',
                            'rating' => '?',
                            'uuid' => '?',
                        ]
                    )
                    ->setParameter(0, $this->getBookTitle())
                    ->setParameter(1, $this->generator->isbn13)
                    ->setParameter(2, $this->generator->name)
                    ->setParameter(3, $this->generator->year)
                    ->setParameter(4, rand(0, 1))
                    ->setParameter(5, rand(0, 1))
                    ->setParameter(6, rand(1, 10))
                    ->setParameter(7, (!empty($this->tripUuids)) ? $this->tripUuids[array_rand($this->tripUuids)] : rand(1, 61))
                    ->setParameter(8, $this->getPrice(15))
                    ->setParameter(9, rand(1, 4))
                    ->setParameter(10, rand(1, 4))
                    ->setParameter(11, $askingPrice)
                    ->setParameter(12, rand(0, 1))
                    ->setParameter(13, rand(0, 50))
                    ->setParameter(14, rand(0, 4))
                    ->setParameter(15, $this->generator->realText(rand(200, 500)))
                    ->setParameter(16, rand(0, 5))
                    ->setParameter(17, $soldDate)
                    ->setParameter(18, $soldPrice)
                    ->setParameter(19, rand(0, 1))
                    ->setParameter(20, rand(1, 5))
                    ->setParameter(21, $this->generator->uuid)
                    ->execute();
            }
        });
    }

    /**
     * Seeds the competition table
     *
     * @throws \Exception
     */
    public function seedCompetition()
    {
        // Truncate table
        $this->truncate('competition', function ($table) {
            // Then add new data
            for ($i = 0; $i <= 200; $i++) {
                $this->connection->createQueryBuilder()
                    ->insert($table)
                    ->values(
                        [
                            'isbn' => '?',
                            'price' => '?',
                            'url' => '?',
                            'last_checked' => '?',
                            'uuid' => '?'
                        ]
                    )
                    ->setParameter(0, $this->generator->isbn13)
                    ->setParameter(1, $this->getPrice())
                    ->setParameter(2, $this->generator->url)
                    ->setParameter(3, $this->generator->date)
                    ->setParameter(4, $this->generator->uuid)
                    ->execute();
            }
        });
    }

    /**
     * Seeds the photo table
     *
     * @throws \Exception
     */
    public function seedPhoto()
    {
        // Truncate table
        $this->truncate('photo', function ($table) {
            // Then add new data
            for ($i = 0; $i <= 200; $i++) {
                $this->connection->createQueryBuilder()
                    ->insert($table)
                    ->values(
                        [
                            'path' => '?',
                            'uuid' => '?',
                        ]
                    )
                    ->setParameter(0, 'assets/img/books/book_' . rand(1, 10) . '.jpg')
                    ->setParameter(1, $this->generator->uuid)
                    ->execute();
            }
        });
    }

    /**
     * Seeds the book_photo table
     *
     * @throws \Exception
     */
    public function seedBookPhoto()
    {
        // Truncate table
        $this->truncate('book_photo', function ($table) {
            // Then add new data
            $pairings = [];

            for ($i = 0; $i <= 360; $i++) {
                $photo_id = rand(1, 201);
                $book_id = rand(1, 121);

                if (!array_key_exists($book_id, $pairings) || !in_array($photo_id, $pairings[$book_id])) {
                    $pairings[$book_id][] = $photo_id;

                    $this->connection->createQueryBuilder()
                        ->insert($table)
                        ->values(
                            [
                                'book_id' => '?',
                                'photo_id' => '?'
                            ]
                        )
                        ->setParameter(0, $book_id)
                        ->setParameter(1, $photo_id)
                        ->execute();
                }
            }
        });
    }

    /**
     * Seeds the book_trip table
     *
     * @throws \Exception
     */
    public function seedBookTrip()
    {
        // Truncate table
        $this->truncate('book_trip', function ($table) {
            // Then add new data
            $pairings = [];

            for ($i = 0; $i <= 80; $i++) {
                $trip_id = rand(1, 61);
                $book_id = rand(1, 121);

                if (!array_key_exists($book_id, $pairings) || !in_array($trip_id, $pairings[$book_id])) {
                    $pairings[$book_id][] = $trip_id;

                    $this->connection->createQueryBuilder()
                        ->insert($table)
                        ->values(
                            [
                                'book_id' => '?',
                                'trip_id' => '?'
                            ]
                        )
                        ->setParameter(0, $book_id)
                        ->setParameter(1, $trip_id)
                        ->execute();
                }
            }
        });
    }

    /**
     * Seeds the trip table
     *
     * @throws \Exception
     */
    public function seedTrip()
    {
        // Truncate table
        $this->truncate('trip', function ($table) {
            // Then add new data
            for ($i = 0; $i <= 60; $i++) {
                $this->tripUuids[] = $uuid = $this->generator->uuid;

                $this->connection->createQueryBuilder()
                    ->insert($table)
                    ->values(
                        [
                            'date' => '?',
                            'price' => '?',
                            'receipt' => '?',
                            'location_id' => '?',
                            'uuid' => '?',
                        ]
                    )
                    ->setParameter(0, $this->generator->date('Y-m-d'))
                    ->setParameter(1, $this->getPrice())
                    ->setParameter(2, 'assets/img/receipts/receipt.jpg')
                    ->setParameter(3, (!empty($this->locationUuids)) ? $this->locationUuids[array_rand($this->locationUuids)] : rand(1, 31))
                    ->setParameter(4, $uuid)
                    ->execute();
            }
        });
    }

    /**
     * Seeds the location table
     *
     * @throws \Exception
     */
    public function seedLocation()
    {
        $this->truncate('location', function ($table) {
            // Then add new data
            for ($i = 0; $i <= 30; $i++) {
                $this->locationUuids[] = $uuid = $this->generator->uuid;

                $this->connection->createQueryBuilder()
                    ->insert($table)
                    ->values(
                        [
                            'name' => '?',
                            'address' => '?',
                            'latitude' => $this->generator->latitude,
                            'longitude' => $this->generator->longitude,
                            'uuid' => '?',
                        ]
                    )
                    ->setParameter(0, $this->generator->company)
                    ->setParameter(1, $this->generator->address)
                    ->setParameter(2, $uuid)
                    ->execute();
            }
        });
    }

    /**
     * Randomly chooses whether the book is sold or not. If it is, then the sold price is less than the asking price
     *
     * @param $sold
     * @param $soldPrice
     *
     * @return array
     */
    private function getSoldDetails($sold, $soldPrice)
    {
        if ($sold) {
            return [
                $this->generator->dateTime->format('Y-m-d H:i:s'),
                $soldPrice
            ];
        }

        return [null, null];
    }

    /**
     * Get random price
     *
     * @param int $max
     *
     * @return string
     */
    private function getPrice($max = 300)
    {
        return number_format(rand(5, $max) . '.' . rand(1, 99), 2);
    }

    /**
     * Truncates the table then uses the callback to add new data
     *
     * @param $table
     * @param $callback
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    private function truncate($table, $callback)
    {
        $this->connection->beginTransaction();
        try {
            $this->connection->query('SET FOREIGN_KEY_CHECKS = 0');
            $this->connection->executeUpdate("TRUNCATE TABLE {$table}");
            $this->connection->query('SET FOREIGN_KEY_CHECKS = 1');

            if (is_callable($callback)) {
                $callback($table);
            }

            $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollback();
            throw $e;
        }
    }

    /**
     * @param int $words
     *
     * @return string
     */
    private function getBookTitle($words = 5)
    {
        $sentence = $this->generator->sentence($words);
        return substr($sentence, 0, strlen($sentence) - 1);
    }
}