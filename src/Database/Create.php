<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Database;

class Create
{
    public function createDatabase()
    {
        $manager = new Manager();
        $connection = $manager->getConnection();
        foreach ($this->tablesSqlArray() as $sql) {
            $connection->executeQuery($sql);
        }
    }

    private function tablesSqlArray()
    {
        return [
            "CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'book title', isbn VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'isbn of book', author VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci, year_printed DATE NOT NULL, signed TINYINT(1) DEFAULT '0' NOT NULL COMMENT 'autographed?', first_edition TINYINT(1) DEFAULT '0' NOT NULL, printing INT NOT NULL, trip_id VARCHAR(255) NOT NULL, cost NUMERIC(10, 2) DEFAULT '0.00' NOT NULL COMMENT 'price paid for book less taxes and s+h', market INT DEFAULT 1 NOT NULL, sale_status INT DEFAULT 1 NOT NULL, sale_price NUMERIC(10, 2) DEFAULT '0.00' NOT NULL, firm TINYINT(1) DEFAULT '0' NOT NULL COMMENT 'firm on price', max_discount INT DEFAULT 0 NOT NULL COMMENT 'percentage of discount', format INT DEFAULT 0 NOT NULL, sold_on DATETIME DEFAULT NULL, sold_price NUMERIC(10, 0) DEFAULT NULL, book_read TINYINT(1) DEFAULT '0' NOT NULL, rating INT DEFAULT 3 NOT NULL, book_condition INT DEFAULT 0 NOT NULL, uuid VARCHAR(155) DEFAULT NULL COLLATE utf8mb4_general_ci, description TEXT NOT NULL COLLATE utf8mb4_general_ci, UNIQUE INDEX uuid (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "CREATE TABLE book_photo (book_id INT NOT NULL, photo_id INT NOT NULL, INDEX photo_id (photo_id), INDEX IDX_1A75916116A2B381 (book_id), PRIMARY KEY(book_id, photo_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "CREATE TABLE book_trip (book_id INT NOT NULL, trip_id INT NOT NULL, PRIMARY KEY(book_id, trip_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "CREATE TABLE competition (id INT AUTO_INCREMENT NOT NULL, isbn VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci, price NUMERIC(10, 2) NOT NULL, url VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci, last_checked DATE DEFAULT NULL, uuid VARCHAR(155) NOT NULL COLLATE utf8mb4_general_ci, UNIQUE INDEX uuid (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci, address VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci, latitude VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci, longitude VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci, uuid VARCHAR(155) NOT NULL COLLATE utf8mb4_general_ci, UNIQUE INDEX uuid (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, path VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci, uuid VARCHAR(155) NOT NULL COLLATE utf8mb4_general_ci, UNIQUE INDEX uuid (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "CREATE TABLE trip (id INT AUTO_INCREMENT NOT NULL, date DATE DEFAULT NULL, price NUMERIC(10, 2) DEFAULT '0.00' NOT NULL COMMENT 'Total price of purchase', receipt VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_general_ci COMMENT 'photo of receipt', location_id VARCHAR(255) NOT NULL, uuid VARCHAR(155) NOT NULL COLLATE utf8mb4_general_ci, UNIQUE INDEX uuid (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB",
            "ALTER TABLE book_photo ADD CONSTRAINT book_photo_ibfk_1 FOREIGN KEY (book_id) REFERENCES book (id) ON UPDATE CASCADE ON DELETE CASCADE",
            "ALTER TABLE book_photo ADD CONSTRAINT book_photo_ibfk_2 FOREIGN KEY (photo_id) REFERENCES photo (id) ON UPDATE CASCADE ON DELETE CASCADE",
        ];
    }
}