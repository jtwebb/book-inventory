<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Transformers;

/**
 * Class TransformerFactory
 * @package FathomFire\Transformers
 * @author John Webb
 * @version 1.0
 */
class TransformerFactory
{
    /**
     * @param string    $entity        The name of the entity sans 'Transformer'
     * @param array     $array         The data to transform
     * @param bool|true $isCollection  Whether the data is an array of arrays or not
     *
     * @return array
     */
    public static function create($entity, array $array, $isCollection = true)
    {
        $transformer = self::checkEntity($entity);

        if ($isCollection) {
            foreach ($array as $key => $value) {
                $array[$key] = $transformer->transform($value);
            }
        } else {
            $array = $transformer->transform($array);
        }

        return $array;
    }

    /**
     * @param string $entity  The name of the entity sans 'Transformer'
     *
     * @return TransformerInterface
     * @throws \InvalidArgumentException if the class doesn't exist
     * @throws \InvalidArgumentException if the class isn't an instance of TransformerInterface
     */
    public static function checkEntity($entity)
    {
        $class = '\\FathomFire\\Transformers\\' . ucfirst($entity) . 'Transformer';
        if (!class_exists($class)) {
            throw new \InvalidArgumentException("The class {$class} does not exist.");
        }

        $transformer = new $class();
        if (!$transformer instanceof TransformerInterface) {
            throw new \InvalidArgumentException("The class {$class} does not exist.");
        }

        return $transformer;
    }
}
