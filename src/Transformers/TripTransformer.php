<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Transformers;

class TripTransformer implements TransformerInterface
{
    /**
     * Transforms the data to the correct type
     *
     * @param array $array
     *
     * @return array
     */
    public function transform(array $array)
    {
        return [
            'date'        => date('r', strtotime($array['date'])),
            'price'       => (float)$array['price'],
            'receipt'     => $array['receipt'],
            'location_id' => $array['location_id'],
            'uuid'        => $array['uuid'],
        ];
    }
}
