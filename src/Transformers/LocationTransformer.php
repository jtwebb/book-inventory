<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Transformers;

class LocationTransformer implements TransformerInterface
{
    /**
     * Transforms the data to the correct type
     *
     * @param array $array
     *
     * @return array
     */
    public function transform(array $array)
    {
        return [
            'name'      => $array['name'],
            'address'   => $array['address'],
            'latitude'  => (float) $array['latitude'],
            'longitude' => (float) $array['longitude'],
            'uuid'      => $array['uuid'],
        ];
    }
}
