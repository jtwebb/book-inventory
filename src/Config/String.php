<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Config;

class String extends AbstractConfig
{
    /**
     * @var null|string
     */
    public $path = BASE_DIR . '/app/strings.php';

    public function __construct($path = null)
    {
        if (null !== $path) {
            $this->path = $path;
        }

        parent::__construct($path);
    }

    /**
     * @param string $keys
     * @param array  $replace
     * @param null   $path
     *
     * @return mixed
     */
    public static function get($keys, array $replace = [], $path = null)
    {
        $config = new self($path);
        $string = $config->retrieveItem($keys);
        if (!empty($replace)) {
            $string = str_replace(array_keys($replace), array_values($replace), $string);
        }
        return $string;
    }

    /**
     * @param string $keys
     * @param null   $path
     *
     * @return bool
     */
    public static function has($keys, $path = null)
    {
        $config = new self($path);
        return $config->hasItem($keys);
    }
}
