<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Config;

class Config extends AbstractConfig
{
    /**
     * @var null|string
     */
    protected $path = BASE_DIR . '/app/config.php';

    public function __construct($path = null)
    {
        if (null !== $path) {
            $this->path = $path;
        }

        parent::__construct($path);
    }

    /**
     * Get configuration options by key. If the options you're trying to get is nested, then separate the
     * keys by a period (e.g. 'key.second_key.third_key').
     *
     * @param string     $keys
     * @param bool|false $checking Set to true if you just need to know if the options exists and don't actually need
     *                             the value returned.
     *
     * @return bool|array
     */
    private function traverse($keys, $checking = false)
    {
        $keys = explode('.', $keys);
        $tempItems = $this->items;

        foreach ($keys as $key) {
            // Oops! the item isn't here!
            if (!array_key_exists($key, $tempItems)) {
                // If we're just checking we want to return a boolean
                if ($checking) {
                    return false;
                }
                // Otherwise we'll just set the return value to an empty array
                $tempItems = [];
                break;
            }

            $tempItems = $tempItems[$key];
        }

        if ($checking) {
            return true;
        }

        return $tempItems;
    }

    /**
     * @param string $keys
     *
     * @return bool
     */
    public function hasItem($keys)
    {
        return $this->traverse($keys, true);
    }

    /**
     * @param string $keys
     *
     * @return mixed
     */
    public function retrieveItem($keys)
    {
        if ($this->hasItem($keys)) {
            return $this->traverse($keys);
        }

        return '';
    }

    /**
     * @param string $keys
     * @param array  $replace
     * @param null   $path
     *
     * @return mixed
     */
    public static function get($keys, array $replace = [], $path = null)
    {
        $config = new self($path);
        return $config->retrieveItem($keys);
    }

    /**
     * @param string $keys
     * @param null   $path
     *
     * @return bool
     */
    public static function has($keys, $path = null)
    {
        $config = new self($path);
        return $config->hasItem($keys);
    }
}
