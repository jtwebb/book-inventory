<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Config;

abstract class AbstractConfig implements ConfigInterface
{
    /**
     * @var array
     */
    protected $items;

    /**
     * @param $path
     *
     * @return \FathomFire\Config\ConfigInterface
     */
    protected static function getClass($path)
    {
    }

    public function __construct($path = null)
    {
        $this->items = $this->retrieveItems();
    }

    /**
     * @return array
     */
    protected function retrieveItems()
    {
        return include $this->path;
    }

    /**
     * @param string $keys
     *
     * @return bool
     */
    public function hasItem($keys)
    {
        return array_key_exists($keys, $this->items);
    }

    /**
     * @param string $keys
     *
     * @return mixed
     */
    public function retrieveItem($keys)
    {
        if ($this->hasItem($keys)) {
            return $this->items[$keys];
        }

        return '';
    }
}
