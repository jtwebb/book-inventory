<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Config;

interface ConfigInterface
{
    public function __construct($path = null);

    /**
     * @param string $keys
     *
     * @return bool
     */
    public function hasItem($keys);

    /**
     * @param string $keys
     *
     * @return mixed
     */
    public function retrieveItem($keys);

    /**
     * @param string $keys
     * @param null   $path
     *
     * @return bool
     */
    public static function has($keys, $path = null);

    /**
     * @param string $keys
     * @param array  $replace
     * @param null   $path
     *
     * @return mixed
     */
    public static function get($keys, array $replace = [], $path = null);
}
