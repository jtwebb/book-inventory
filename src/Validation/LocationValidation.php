<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Validation;

use FathomFire\Config\String;
use Symfony\Component\Validator\Constraints as Assert;

class LocationValidation extends AbstractValidation
{
    /**
     * @param array $input
     * @param bool  $update
     *
     * @return bool
     */
    public function isValid(array $input, $update = false)
    {
        $fields = [
            'name' => new Assert\NotBlank(['message' => String::get('location_name_blank_error')]),
            'address' => new Assert\NotBlank(['message' => String::get('location_address_blank_error')]),
        ];

        if ($update) {
            $fields['uuid'] = new Assert\NotBlank();
            $fields['uuid']->message = String::get('location_uuid_blank_error');
        }

        return $this->processViolations($fields, $input);
    }
}
