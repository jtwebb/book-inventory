<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Validation;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractValidation implements ValidationInterface
{
    /**
     * @var \Symfony\Component\Validator\Validator\RecursiveValidator|\Symfony\Component\Validator\ValidatorInterface
     */
    protected $validator;

    /**
     * @var ConstraintViolationListInterface
     */
    protected $violations;

    public function __construct()
    {
        $this->validator = ValidationFactory::createConstraintValidator();
    }

    /**
     * @return array
     */
    public function getViolations()
    {
        return $this->violations;
    }

    /**
     * @param $fields
     * @param $input
     *
     * @return bool
     */
    protected function processViolations($fields, $input)
    {
        $constraint = new Assert\Collection($fields);

        $violations = $this->validator->validate($input, $constraint);
        $count = $violations->count();

        $isValid = $count < 1;

        if ($count > 0) {
            while ($count--) {
                $this->violations['data']['error'][] = $violations->get($count)->getMessage();
            }
        }

        return $isValid;
    }
}
