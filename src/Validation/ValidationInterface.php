<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Validation;

interface ValidationInterface
{
    /**
     * @param array $input
     * @param bool  $update
     *
     * @return bool
     */
    public function isValid(array $input, $update = false);

    /**
     * @return array
     */
    public function getViolations();
}
