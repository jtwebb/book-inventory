<?php
/**
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Validation;

use Symfony\Component\Validator\ValidatorBuilder;

class ValidationFactory
{
    /**
     * @param $validator
     *
     * @return ValidationInterface
     * @throws \InvalidArgumentException
     */
    public static function create($validator)
    {
        $class = __NAMESPACE__ . '\\' . ucfirst(strtolower($validator)) . 'Validation';
        if (class_exists($class)) {
            return new $class();
        }

        throw new \InvalidArgumentException("The class {$class} does not exist");
    }

    /**
     * @return \Symfony\Component\Validator\Validator\RecursiveValidator
     *         |\Symfony\Component\Validator\ValidatorInterface
     */
    public static function createConstraintValidator()
    {
        $builder = new ValidatorBuilder();
        return $builder->getValidator();
    }
}
