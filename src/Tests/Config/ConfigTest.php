<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Tests\Config;

use FathomFire\Config\Config;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    protected $config;

    public function setUp()
    {
        $this->config = new Config(dirname(__DIR__) . '/mocks/config/config.php');
    }

    public function testHasConfigItemReturnsFalse()
    {
        $firstValue  = $this->config->hasItem('foo');
        $secondValue = $this->config->hasItem('array.bar');
        $thirdValue  = $this->config->hasItem('nestedArray.secondArray.baz');

        $this->assertFalse($firstValue);
        $this->assertFalse($secondValue);
        $this->assertFalse($thirdValue);
    }

    public function testHasConfigItemReturnsTrue()
    {
        $firstValue  = $this->config->hasItem('singleValue');
        $secondValue = $this->config->hasItem('array.arrayValue');
        $thirdValue  = $this->config->hasItem('nestedArray.secondArray.thirdArray');

        $this->assertTrue($firstValue);
        $this->assertTrue($secondValue);
        $this->assertTrue($thirdValue);
    }

    public function testRetrieveItemReturnsCorrectValue()
    {
        $firstValue  = $this->config->retrieveItem('singleValue');
        $secondValue = $this->config->retrieveItem('array.arrayValue');
        $thirdValue  = $this->config->retrieveItem('nestedArray.secondArray.thirdArray');
        $fourthValue = $this->config->retrieveItem('nestedArray.secondArray');
        $fifthValue  = $this->config->retrieveItem('object');

        $this->assertEquals('SingleValue', $firstValue);
        $this->assertEquals('ArrayValue', $secondValue);
        $this->assertEquals('ThirdArray', $thirdValue);
        $this->assertTrue(is_array($fourthValue));
        $this->assertArrayHasKey('thirdArray', $fourthValue);
        $this->assertInstanceOf('stdClass', $fifthValue);
    }
}
