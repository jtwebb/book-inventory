Feature: Locations

Scenario: Returning a collection of places
    When I request "GET /locations"
    Then I get a "200" response
    And scope into the first "data" property
        And the properties exist:
            """
            name
            latitude
            longitude
            address
            uuid
            """
        And the "latitude" property is a float

Scenario: Finding a specific place
    When I request "GET /locations/id/2d23eb20-db54-37b0-bde6-1255cbaac32f"
    Then I get a "200" response
    And scope into the "data" property
        And the properties exist:
            """
            name
            latitude
            longitude
            address
            uuid
            """
        And the "latitude" property is a float

Scenario: Finding places by latitude
    When I request "GET /locations/lat/36.825669"
    Then I get a "200" response
    And scope into the first "data" property
        And the properties exist:
            """
            name
            latitude
            longitude
            address
            uuid
            """
        And the "latitude" property is a float

Scenario: Finding places by longitude
    When I request "GET /locations/lng/-45.235577"
    Then I get a "200" response
    And scope into the first "data" property
        And the properties exist:
            """
            name
            latitude
            longitude
            address
            uuid
            """
        And the "latitude" property is a float

Scenario: Finding places by latitude and longitude
    When I request "GET /locations/address/71.089861/-5.881475"
    Then I get a "200" response
    And scope into the first "data" property
        And the properties exist:
            """
            name
            latitude
            longitude
            address
            uuid
            """
        And the "latitude" property is a float

Scenario: Finding places by trip id
    When I request "GET /locations/trip/d12a8a0a-a6ff-3290-bfbd-24a028eeab75"
    Then I get a "200" response
    And scope into the "data" property
        And the properties exist:
            """
            name
            latitude
            longitude
            address
            uuid
            """
        And the "latitude" property is a float



Scenario: Saving a location
    Given I have the payload:
    """
    {"name": "Empire State Building", "address": "350 5th Avenue, New York, NY 10118"}
    """
    When I request "POST /locations/add"
    Then I get a "201" response
    And scope into the "data" property
    And the properties exist:
        """
        insert_id
        """
    And the "insert_id" property is an integer


Scenario: Updating a location
    Given I have the payload:
    """
    {"name": "Empire State", "address": "350 5th Avenue, New York, NY 10118", "uuid": "825d3a89-e2f5-5e85-a82f-2f54565a2d8d"}
    """
    When I request "PUT /locations/update"
    Then I get a "200" response
    And scope into the "data" property
    And the properties exist:
        """
        rows_affected
        """
    And the "rows_affected" property is an integer
