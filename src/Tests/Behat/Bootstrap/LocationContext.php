<?php

namespace FathomFire\Tests\Behat\Bootstrap;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

/**
 * Defines application features from the specific context.
 */
class LocationContext extends \PHPUnit_Framework_TestCase implements Context, SnippetAcceptingContext
{
    /**
     * The Guzzle HTTP Client.
     */
    protected $client;

    /**
     * The current resource
     */
    protected $resource;

    /**
     * The request payload
     */
    protected $requestPayload;

    /**
     * The Guzzle HTTP Response.
     */
    protected $response;

    /**
     * The decoded response object.
     */
    protected $responsePayload;

    /**
     * The current scope within the response payload
     * which conditions are asserted against.
     */
    protected $scope;

    protected $payloadString;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param      $baseUrl
     * @param null $guzzle
     */
    public function __construct($baseUrl, $guzzle = null)
    {
        $config = $guzzle !== null && is_array($guzzle) ? $guzzle : [];
        $config['base_uri'] = $baseUrl;
        $this->client = new Client($config);
        parent::__construct();
    }

    /**
     * @When /^I request "(GET|POST|PUT|DELETE) ([^"]*)"$/
     *
     * @param $httpMethod
     * @param $resource
     */
    public function iRequest($httpMethod, $resource)
    {
        $this->resource = $resource;
        $method = strtolower($httpMethod);

        try {
            switch ($httpMethod) {
                case 'PUT':
                case 'POST':
                    $this->response = $this->client->{$method}($resource, [
                        'form_params' => json_decode($this->requestPayload, true)
                    ]);
                    break;

                default:
                    $this->response = $this->client->{$method}($resource);
            }
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();
        }
    }

    /**
     * @Then /^I get a "([^"]*)" response$/
     *
     * @param $statusCode
     */
    public function iGetResponse($statusCode)
    {
        $response = $this->getResponse();
        $contentType = $response->getHeader('Content-Type');

        if ($contentType[0] === 'application/json') {
            $bodyOutput = $response->getBody();
        } else {
            $bodyOutput = sprintf(
                'Output is %s which is not JSON and is therefore scary. Run the request manually.',
                $contentType[0]
            );
        }

        $this->assertSame((int)$statusCode, (int)$this->getResponse()->getStatusCode(), $bodyOutput);
    }

    /**
     * @Given /^scope into the first "([^"]*)" property$/
     * @param $scope
     */
    public function scopeIntoTheFirstProperty($scope)
    {
        $this->scope = "{$scope}.0";
    }

    /**
     * @Given /^scope into the "([^"]*)" property$/
     *
     * @param $scope
     */
    public function scopeIntoTheProperty($scope)
    {
        $this->scope = $scope;
    }

    /**
     * @Given /^the properties exist:$/
     *
     * @param \Behat\Gherkin\Node\PyStringNode $propertiesString
     */
    public function thePropertiesExist(PyStringNode $propertiesString)
    {
        foreach (explode("\n", (string)$propertiesString) as $property) {
            $this->thePropertyExists($property);
        }
    }

    /**
     * @Given /^the "([^"]*)" property exists/
     * @param $property
     */
    public function thePropertyExists($property)
    {
        $payload = $this->getScopePayload();
        $message = sprintf(
            'Asserting the [%s] property exists in the scope [%s]: [%s]',
            $property,
            $this->scope,
            $this->payloadString
        );

        $this->assertTrue(array_key_exists($property, $payload), $message);
    }

    /**
     * @Given /^the "([^"]*)" property is an integer$/
     * @param $property
     */
    public function thePropertyIsAnInteger($property)
    {
        $payload = $this->getScopePayload();

        $this->isType(
            'int',
            $this->arrayGet($payload, $property),
            sprintf(
                "Asserting the [%s] property in current scope [%s] is an integer: %s",
                $property,
                $this->scope,
                $this->response->getBody()
            )
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a float$/
     * @param $property
     */
    public function thePropertyIsAFloat($property)
    {
        $payload = $this->getScopePayload();

        $this->isType(
            'float',
            $this->arrayGet($payload, $property),
            sprintf(
                "Asserting the [%s] property in current scope [%s] is a float: %s",
                $property,
                $this->scope,
                json_encode($payload)
            )
        );
    }

    /**
     * Return the response payload from the current response.
     *
     * @return mixed
     * @throws \Exception
     */
    protected function getResponsePayload()
    {
        if (!$this->responsePayload) {
            $this->payloadString = $this->getResponse()->getBody();
            $json = json_decode($this->getResponse()->getBody(), true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                $message = 'Failed to decode JSON body ';

                switch (json_last_error()) {
                    case JSON_ERROR_DEPTH:
                        $message .= '(Maximum stack depth exceeded).';
                        break;
                    case JSON_ERROR_STATE_MISMATCH:
                        $message .= '(Underflow or the modes mismatch).';
                        break;
                    case JSON_ERROR_CTRL_CHAR:
                        $message .= '(Unexpected control character found).';
                        break;
                    case JSON_ERROR_SYNTAX:
                        $message .= '(Syntax error, malformed JSON).';
                        break;
                    case JSON_ERROR_UTF8:
                        $message .= '(Malformed UTF-8 characters, possibly incorrectly encoded).';
                        break;
                    default:
                        $message .= '(Unknown error).';
                        break;
                }

                throw new \Exception($message);
            }

            $this->responsePayload = $json;
        }

        return $this->responsePayload;
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param $array
     * @param $key
     *
     * @return mixed
     */
    protected function arrayGet($array, $key)
    {
        if (is_null($key)) {
            return $array;
        }

        foreach (explode('.', $key) as $segment) {
            if (!array_key_exists($segment, $array)) {
                return false;
            }

            $array = $array[$segment];
        }

        return $array;
    }

    /**
     * Returns the payload from the current scope within the response
     *
     * @return mixed
     */
    protected function getScopePayload()
    {
        $payload = $this->getResponsePayload();

        if (!$this->scope) {
            return $payload;
        }

        return $this->arrayGet($payload, $this->scope);
    }

    /**
     * @Given /^I have the payload:$/
     *
     * @param \Behat\Gherkin\Node\PyStringNode $requestPayload
     */
    public function iHaveThePayload(PyStringNode $requestPayload)
    {
        $this->requestPayload = $requestPayload;
    }

    /**
     * Checks the response exists and returns it.
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    protected function getResponse()
    {
        if (!$this->response) {
            throw new \Exception('You must first make a request to check a response');
        }

        return $this->response;
    }
}
