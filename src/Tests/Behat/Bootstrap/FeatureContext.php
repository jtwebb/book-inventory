<?php

namespace FathomFire\Tests\Behat\Bootstrap;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\Client;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    protected $client;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $config = isset($params['guzzle']) && is_array($params['guzzle']) ? $params['guzzle'] : [];
        $config['base_url'] = $params['base_url'];
        $this->client = new Client($config);
    }
}
