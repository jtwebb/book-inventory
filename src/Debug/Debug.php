<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Debug;

use Psr\Log\LoggerInterface;

class Debug
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $before = '<pre>';

    /**
     * @var string
     */
    protected $after = '</pre>';

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $data
     *
     * @return null|string
     */
    public function dump($data)
    {
        ob_start();
        var_dump($data);
        $data = ob_get_contents();
        ob_end_clean();

        return $this->decider($this->wrap($data), true);
    }

    /**
     * @param $exception
     *
     * @throws \Exception
     */
    public function errorException($exception)
    {
        if ($exception instanceof \Exception || is_subclass_of($exception, 'Exception')) {
            $result = $this->decider($exception);
            if ($result !== null) {
                throw $exception;
            }
        }
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function wrap($data)
    {
        return $this->before . $data . $this->after;
    }

    /**
     * @param string     $return The value to either return or log
     * @param bool|false $echo
     *
     * @return null
     */
    protected function decider($return, $echo = false)
    {
        if (DEBUG) {
            echo $return;
            if ($echo) {
                echo $return;
            }
            return $return;
        }

        $this->logger->debug(var_export($return, true));
        return null;
    }
}