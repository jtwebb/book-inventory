<?php
/*
 * This file is part of the book_inventory package.
 *
 * (c) John Webb <john@fathomfire.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FathomFire\Debug;

use FathomFire\Logger\LoggerFactory;
use Monolog\Logger;

class DebugFactory
{
    public static function create()
    {
        return new Debug(LoggerFactory::create('DebugLogger', 'debug', Logger::DEBUG));
    }
}